# Gradle Gitlab Generic Packages Plugin

Simple Gradle plugin that uploads and downloads Gitlab Generic Packages files. 

## Setup

Add the following to your build.gradle file:

```groovy
plugins {
  id 'com.fcavalieri.gradle.gitlab-generic-packages' version '1.0.0'
}
```

# Usage

## Authentication

The S3 plugin searches for credentials in the same order and location as the [Gradle Gitlab Credentials plugin](https://gitlab.com/fcavalieri/gradle-gitlab-credentials).

## Tasks

The following Gradle tasks are provided: GitlabUpload, GitlabDownload. They have the following common properties:

+ `String host` - Gitlab host (e.g. https://gitlab.reportix.com). Optional, it is extracted from the repository remote if unspecified  
+ `String projectId` - Gitlab projectId (e.g. 123456). Optional, it is determined using the repository remote and Gitlab API if unspecified
+ `String packageName` - Package name (e.g. mylibrary). Optional, defaults to the project name
+ `String packageVersion` - Package version (e.g. 0.0.1). Optional, defaults to the project version
+ `boolean overwrite` - Whether to overwrite the destination file. Optional, defaults to false

### GitlabUpload

+ `String remotePath` - Remote path (e.g. myfile.zip). Optional, defaults to the localPath filename.
+ `Path localPath` - Local path (absolute) of the file to upload

Example:
```groovy
plugins {
    id('com.fcavalieri.gradle.gitlab-generic-packages')
}

import java.nio.file.Paths
import com.fcavalieri.gradle.GitlabUpload
tasks.register("upload", GitlabUpload) {
  localPath = project.rootDir.toPath().resolve("resource.zip")
}
```

### S3Download

+ `String remotePath` - Remote path (e.g. myfile.zip).
+ `Path localPath` - Local path (absolute) of the file to download

```groovy
plugins {
    id('com.fcavalieri.gradle.gitlab-generic-packages')
}

import java.nio.file.Paths
import com.fcavalieri.gradle.GitlabDownload
tasks.register("download", GitlabDownload) {
    remotePath = "resource.zip"
    localPath = project.rootDir.toPath().resolve("resource.zip")
}
```
