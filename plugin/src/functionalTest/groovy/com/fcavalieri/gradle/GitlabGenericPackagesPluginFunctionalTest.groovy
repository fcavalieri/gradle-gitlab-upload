package com.fcavalieri.gradle

import org.gradle.internal.impldep.org.apache.commons.io.FileUtils
import spock.lang.Specification
import spock.lang.TempDir
import org.gradle.testkit.runner.GradleRunner

import java.nio.file.Files
import java.nio.file.Paths

class GitlabGenericPackagesPluginFunctionalTest extends Specification {
    @TempDir
    private File projectDir

    private getBuildFile() {
        new File(projectDir, "build.gradle")
    }

    private getSettingsFile() {
        new File(projectDir, "settings.gradle")
    }

    def "can run task"() {
        FileUtils.copyDirectory(Paths.get("../.git").toFile(), projectDir)

        given:
        settingsFile << ""
        buildFile << """
plugins {
    id('com.fcavalieri.gradle.gitlab-generic-packages')
}

version = "0.0.1"

import java.nio.file.Paths
import com.fcavalieri.gradle.GitlabUpload
import com.fcavalieri.gradle.GitlabDownload
tasks.register("upload", GitlabUpload) {
  localPath = Paths.get("${projectDir}/build.gradle")
}
tasks.register("download", GitlabDownload) {
  dependsOn upload
  
  remotePath = "build.gradle"
  localPath = Paths.get("${projectDir}/build.gradle.downloaded")
}
"""

        when:
        def runner = GradleRunner.create()
        runner.forwardOutput()
        runner.withPluginClasspath()
        runner.withArguments("download")
        runner.withProjectDir(projectDir)
        def result = runner.build()

        then:
        result.output.contains("BUILD SUCCESSFUL")
        Files.readString(Paths.get("${projectDir}/build.gradle")) == Files.readString(Paths.get("${projectDir}/build.gradle.downloaded"))

        //TODO: Real test
    }
}
