package com.fcavalieri.gradle

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.ajoberstar.grgit.gradle.GrgitService
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.Optional
import org.ajoberstar.grgit.gradle.GrgitServiceExtension
import com.fcavalieri.gradle.GitLabCredentialsExtension
import org.gradle.internal.Pair

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.text.DecimalFormat
import org.gradle.api.logging.Logger

import java.util.regex.Matcher
import java.util.regex.Pattern

class GitlabGenericPackagesPlugin implements Plugin<Project> {

    void apply(Project target) {
        target.tasks.register("gitlabUpload", GitlabUpload)
        target.tasks.register("gitlabDownload", GitlabDownload)
    }
}

abstract class GitlabTask extends DefaultTask {

    @Input
    @Optional
    String host

    @Input
    @Optional
    String projectId

    @Input
    @Optional
    String packageName

    @Input
    @Optional
    String packageVersion

    @Input
    boolean overwrite = false

    @Internal
    Provider<GrgitService> serviceProvider = project.getGradle().getSharedServices().registerIfAbsent("grgit", GrgitService.class, spec -> {
        spec.getParameters().getCurrentDirectory().set(project.getLayout().getProjectDirectory())
        spec.getParameters().getInitIfNotExists().set(false)
        spec.getMaxParallelUsages().set(1)
    })

    @Internal
    ObjectMapper objectMapper = new ObjectMapper();

    @Internal
    GitLabCredentialsExtension creds = new GitLabCredentialsExtension(project)

    def void initValues() {
        if (host != null && projectId != null)
            return

        def grgit = serviceProvider.get().getGrgit()
        def remoteUrl = grgit.getRemote().list().get(0).url
        Pair<String, String> parsedGitUrl = parseGitUrl(remoteUrl)
        if (host == null)
        {
            host = parsedGitUrl.getLeft()
            logger.info("Using ${host} as gitlab host")
        }
        if (projectId == null)
        {
            def url = new URL("${host}/api/v4/projects/${URLEncoder.encode(parsedGitUrl.getRight(), "UTF-8")}")
            HttpURLConnection connection = (HttpURLConnection) url.openConnection()
            creds.apply(connection)
            connection.setRequestMethod("GET")
            connection.connect()
            def connectionRC = connection.getResponseCode();
            if (!connectionRC.equals(200)) {
                throw new GradleException("Upload failure: " + connection.getInputStream().text)
            }
            ObjectNode parsedResponse = objectMapper.readValue(connection.getInputStream().text, ObjectNode.class)
            projectId = parsedResponse.get("id").asText()
            logger.info("Using ${projectId} as project id")
        }
        if (packageName == null) {
            packageName = project.getName()
            logger.info("Using ${packageName} as package name")
        }
        if (packageVersion == null) {
            packageVersion = project.getVersion()
            logger.info("Using ${packageVersion} as package version")
        }
    }

    def Pair<String, String> parseGitUrl(String gitUrl) {
        String hostNamePatternStr = "^(([a-zA-Z0-9]+@([a-zA-Z0-9.]+))|([a-zA-Z0-9]+://([a-zA-Z0-9]+@)?([^:/]+)(:[0-9]+)?))[:/](.*)\\.git\$"
        Pattern hostNamePattern = Pattern.compile(hostNamePatternStr)

        Matcher matcher = hostNamePattern.matcher(gitUrl)

        if (matcher.find()) {
            def hostName = (matcher.group(3) != null ? matcher.group(3) : matcher.group(6))
            return new Pair<String, String>("https://" + hostName, matcher.group(8))
        }

        throw new Exception("Cannot identify git hostname and repository path from git url " + gitUrl)
    }
}


class GitlabUpload extends GitlabTask {
    @Input
    @Optional
    String remotePath

    @InputFile
    Path localPath

    def void initValues() {
        if (remotePath == null) {
            remotePath = localPath.getFileName()
            logger.info("Using ${remotePath} as remote path")
        }
    }

    @TaskAction
    def task() {
        super.initValues()
        initValues()

        def url = new URL("${host}/api/v4/projects/${projectId}/packages/generic/${packageName}/${packageVersion}/${remotePath}")
        logger.info("Release URL: ${url}")
        if (!overwrite) {
            HttpURLConnection headConnection = (HttpURLConnection) url.openConnection()
            creds.apply(headConnection)
            headConnection.setRequestMethod("HEAD")
            headConnection.connect()
            def headConnectionRC = headConnection.getResponseCode();
            if (headConnectionRC == 200) {
                throw new GradleException("A release for this version and file already exists")
            } else if (headConnectionRC == 404) {
                logger.info("A release for this version and file does not exist")
            } else {
                throw new GradleException("Unable to verify if a release for this version already exists, status code: ${headConnectionRC}")
            }
        }

        HttpURLConnection putConnection = (HttpURLConnection) url.openConnection()
        putConnection.setRequestMethod('PUT')
        putConnection.setDoOutput(true)
        creds.apply(putConnection)
        Files.copy(localPath, putConnection.outputStream)
        def putConnectionRC = putConnection.getResponseCode();
        if (putConnectionRC != 201 && putConnectionRC != 200) {
            throw new GradleException("Upload failure: " + putConnection.getInputStream().text)
        }
        logger.info("Uploaded ${url}")
    }
}


class GitlabDownload extends GitlabTask {

    @Input
    String remotePath

    @OutputFile
    Path localPath

    @TaskAction
    def task() {
        super.initValues()

        if (!overwrite && Files.exists(localPath)) {
            throw new GradleException("Unable to download, local file already exists")
        }

        def url = new URL("${host}/api/v4/projects/${projectId}/packages/generic/${packageName}/${packageVersion}/${remotePath}")
        logger.info("Release URL: ${url}")

        HttpURLConnection getConnection = (HttpURLConnection) url.openConnection()
        creds.apply(getConnection)
        getConnection.setRequestMethod("GET")
        getConnection.connect()
        def getConnectionRC = getConnection.getResponseCode();
        if (getConnectionRC == 404) {
            throw new GradleException("A release for this version and file does not exist")
        } else if (getConnectionRC != 200) {
            throw new GradleException("Unable to download a release for this version and file")
        }

        Files.createDirectories(localPath.getParent())
        Files.copy(getConnection.getInputStream(), localPath, StandardCopyOption.REPLACE_EXISTING)
        logger.info("Downloaded ${url}")
    }

}
